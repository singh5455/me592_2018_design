# Git Repository for M E 592X (Spring 2018)

**Theme Group:** Design and Manufacturing

**Group Members:**

* Onur Bingol
* Emily Johnson
* Nathan Scheirer
* Aayush Sharma
* Rahul Singh

## Assignments

* [Assignment 1](assignment1/)
* [Assignment 2](assignment2/)
