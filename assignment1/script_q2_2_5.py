import numpy as np

# Conditional loading of matplotlib for HPC cluster execution
mpl_loaded = True
try:
    import matplotlib.pyplot as plt
except ImportError:
    mpl_loaded = False

# Read from CSV using NumPy
data = np.genfromtxt('energydata_complete.csv',
                     delimiter=',',
                     skip_header=1,
                     dtype=None,  # determine type of the columns from the data itself
                     usecols=(-6, -1),  # use the first and the last columns
                     names=['Press_mm_Hg', 'EnergyConsumption'])

# Get energy consumption as a NumPy array
PmH = data['Press_mm_Hg']
EC = data['EnergyConsumption']

#
# Plotting appliances energy consumption vs. Press_mm_Hg
#

# Combine pressure and energy consumption data for plotting
PE = np.array([PmH, EC]).T

# Sort by Press_mm_Hg
#PE.sort(axis=0)

if mpl_loaded:
    # Plot using Matplotlib
    plt.figure(figsize=(10.67, 8), dpi=96)
    plt.scatter(PE[:, 1], PE[:, 0], color='green', s=0.1)
    plt.ylabel('Presssure (mm-Hg)')
    plt.xlabel('Energy Consumption')
    plt.show()
