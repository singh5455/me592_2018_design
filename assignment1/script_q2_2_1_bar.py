import numpy as np
from datetime import datetime

# Conditional loading of matplotlib for HPC cluster execution
mpl_loaded = True
try:
    import matplotlib.pyplot as plt
except ImportError:
    mpl_loaded = False
    
#
# Read CSV file and do data pre-processing
#

# Read from CSV using NumPy
data = np.genfromtxt('energydata_complete.csv',
                     delimiter=',',
                     skip_header=1,
                     dtype=None,  # determine type of the columns from the data itself
                     usecols=(0, -1),  # use the first and the last columns
                     names=['date', 'EnergyConsumption'])

# Convert NumPY bytes array to Python datetime list
date_bytes = data['date']
datelist = []
for db in date_bytes:
    datelist.append(datetime.strptime(db.decode('UTF-8'), '%m/%d/%Y %H:%M'))

# Get energy consumption as a NumPy array
EC = data['EnergyConsumption']

#
# Plotting all period - daily
#

# Create an empty array
all_period = [0.0 for x in range(0, 7)]

# Sum up the same weekdays for the bar chart
for date, econs in zip(datelist, EC):
    key = date.weekday()
    all_period[key] += econs

# Prepare variables for plotting
ap_x = np.arange(1, len(all_period) + 1, dtype=np.int)
ap_y = np.array(all_period)
ap_labels = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']

if mpl_loaded:
    # Plot using Matplotlib
    plt.figure(figsize=(10.67, 8), dpi=96)
    plt.bar(ap_x.T, ap_y.T, color='green', label='Energy Consumption')
    plt.xlabel('Weeks')
    plt.ylabel('Energy Consumption')
    plt.xlim(0.5, len(all_period) + 0.5)
    plt.xticks(ap_x, ap_labels)
    plt.title("Energy Consumption - All Period (Daily)")
    plt.show()

#
# Plotting all period - weekly
#

week_num = datelist[0].isocalendar()[1]
start_date = None
start_count = False
econs_week = 0
econs_list = []

# Sum up the same weekdays for the bar chart
for date, econs in zip(datelist, EC):
    # key = date.weekday()
    # all_period[key] += econs
    if week_num == date.isocalendar()[1]:
            econs_week += econs
    else:
        econs_list.append(econs_week)
        econs_week = econs
        week_num = date.isocalendar()[1]

econs_list.append(econs_week)

# Prepare variables for plotting
ap_x = np.arange(1, len(econs_list) + 1, dtype=np.int)
ap_y = np.array(econs_list)

if mpl_loaded:
    # Plot using Matplotlib
    plt.figure(figsize=(10.67, 8), dpi=96)
    plt.bar(ap_x.T, ap_y.T, color='green', label='Energy Consumption')
    plt.xlabel('Weeks')
    plt.ylabel('Energy Consumption')
    plt.xlim(0.5, len(econs_list) + 0.5)
    plt.xticks(ap_x)
    plt.title("Energy Consumption - All Period (Weekly)")
    plt.show()

#
# Plotting a week
# Choose week using the variable 'week_delta'
#

# Initialize some temporary variables for data processing
econs_list = []  # stores energy consumptions
current_date = datelist[0]
starting_week = datelist[0].isocalendar()[1]
week_delta = 1  # choose any week
econs_week = 0
xlabels_list = []  # label list for plotting
start_date = None
start_count = False

# Find daily energy consumption for the chosen week
for date, econs in zip(datelist, EC):
    if starting_week + week_delta == date.isocalendar()[1]:
        if not start_count:
            start_date = date
            current_date = date
            start_count = True
        if current_date.day == date.day:
            econs_week += econs
        else:
            econs_list.append(econs_week)
            econs_week = econs
            xlabels_list.append(current_date.strftime('%a'))
            current_date = date

# Looks a little bit ood but Python stores the last value in memory for a while  and we can take advantage of that
# Add the last parameter to the list
econs_list.append(econs_week)
xlabels_list.append(current_date.strftime('%a'))

# Prepare processed data for plotting
X = np.arange(1, len(econs_list) + 1, dtype=np.int)
Y = np.array(econs_list)

if mpl_loaded:
    # Plot using Matplotlib
    plt.figure(figsize=(10.67, 8), dpi=96)
    plt.bar(X.T, Y.T, color='green', label='Energy Consumption')
    plt.xlabel('Days of Week')
    plt.ylabel('Energy Consumption')
    plt.xlim(0.5, len(econs_list) + 0.5)
    plt.xticks(X, xlabels_list)
    plt.title("Energy Consumption from " + start_date.strftime('%m/%d/%Y') + " to " + current_date.strftime('%m/%d/%Y'))
    plt.show()
