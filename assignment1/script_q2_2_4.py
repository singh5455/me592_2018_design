import numpy as np
from datetime import datetime

# Conditional loading of matplotlib for HPC cluster execution
mpl_loaded = True
try:
    import matplotlib.pyplot as plt
except ImportError:
    mpl_loaded = False

#
# Read CSV file and do data pre-processing
#

# Read from CSV using NumPy
data = np.genfromtxt('energydata_complete.csv',
                     delimiter=',',
                     skip_header=1,
                     dtype=None,  # determine type of the columns from the data itself
                     usecols=(0, -1),  # use the first and the last columns
                     names=['date', 'EnergyConsumption'])

# Convert NumPY bytes array to Python datetime list
date_bytes = data['date']
datelist = []
for db in date_bytes:
    datelist.append(datetime.strptime(db.decode('UTF-8'), '%m/%d/%Y %H:%M'))

# Get energy consumption as a NumPy array
EC = data['EnergyConsumption']

#
# NSM variable construction and plotting
#

# Initialize a list for creating NSM
nsm_econs = []

# Create NSM vs energy consumption
for date, econs in zip(datelist, EC):
    temp = [0, 0.0]
    temp[0] = date.hour * 3600 + date.minute * 60 + date.second
    temp[1] = econs
    nsm_econs.append(temp)

NSM_EC = np.array(nsm_econs)

# Plot histogram
if mpl_loaded:
    plt.figure(figsize=(10.67, 8), dpi=96)
    plt.hist(NSM_EC[:, 0], normed=1, facecolor='blue', alpha=0.75, bins=20)
    plt.xlabel('NSM')
    plt.title('Histogram of NSM')
    plt.grid()
    plt.show()

# Plot scatter graph
if mpl_loaded:
    plt.figure(figsize=(10.67, 8), dpi=96)
    plt.scatter(NSM_EC[:, 1], NSM_EC[:, 0], color='green', s=0.5)
    plt.xlabel('Energy Consumption')
    plt.ylabel('NSM')
    plt.show()
