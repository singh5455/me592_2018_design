import numpy as np
from datetime import datetime

# Conditional loading of matplotlib for HPC cluster execution
mpl_loaded = True
try:
    import matplotlib.pyplot as plt
except ImportError:
    mpl_loaded = False

# Read from CSV using NumPy
data = np.genfromtxt('energydata_complete.csv',
                     delimiter=',',
                     skip_header=1,
                     dtype=None,  # determine type of the columns from the data itself
                     usecols=(0, 1, -1),  # use the first and the last columns
                     names=['date', 'Appliances', 'EnergyConsumption'])

# Convert NumPY bytes array to Python datetime list
date_bytes = data['date']
datelist = []
for db in date_bytes:
    datelist.append(datetime.strptime(db.decode('UTF-8'), '%m/%d/%Y %H:%M'))

# Get energy consumption as a NumPy array
EC = data['EnergyConsumption']
A = data['Appliances']

# Initialize some temporary variables for data processing
econs_list = []  # stores energy consumptions
appl_list = []
current_date = datelist[0]
starting_week = datelist[0].isocalendar()[1]
week_delta = 1  # choose any week
econs_week = 0
appl_week = 0
start_date = None
start_count = False

# Find daily energy consumption for the chosen week
for date, econs, appl in zip(datelist, EC, A):
    if starting_week + week_delta == date.isocalendar()[1]:
        if not start_count:
            start_date = date
            current_date = date
            start_count = True
        if current_date.hour == date.hour:
            econs_week += econs
            appl_week += appl
        else:
            econs_list.append(econs_week)
            appl_list.append(appl_week)
            econs_week = econs
            appl_week = appl
            current_date = date

# Looks a little bit ood but Python stores the last value in memory for a while  and we can take advantage of that
# Add the last parameter to the list
econs_list.append(econs_week)
appl_list.append(appl_week)


# # Weekly consumption
# for date, econs in zip(datelist, EC):
#     if current_date.isocalendar()[1] == date.isocalendar()[1]:
#         econs_week += econs
#     else:
#         econs_list.append(econs_week)
#         current_date = date
#         econs_week = econs

# Prepare processed data for plotting
X = np.arange(1, len(econs_list)+1, dtype=np.int)
Y = np.array(econs_list)
Z = np.array(appl_list)

appliances = np.array(A).T

# Create heatmap
heatmap, xedges, yedges = np.histogram2d(X, Y, bins=(24, 7))
extent = [1, 7, 0, 24]

if mpl_loaded:
    # Plot using Matplotlib
    plt.figure(figsize=(10.67, 8), dpi=96)
    plt.imshow(heatmap, extent=extent, cmap='YlOrRd')
    plt.xlabel('Days of Week')
    plt.ylabel('Hours of Day')
    plt.yticks(np.arange(0.5, 24.5, 1), np.arange(0, 24, 1))
    plt.tick_params(axis='x', which='both', bottom='off', top='off', labelbottom='off')
    plt.tick_params(axis='y', left='off')
    plt.title("Energy Consumption from " + start_date.strftime('%m/%d/%Y') + " to " + current_date.strftime('%m/%d/%Y'))
    plt.colorbar(ticks=[])
    plt.tight_layout()
    plt.show()
