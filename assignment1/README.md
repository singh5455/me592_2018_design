# M E 592X - Assignment 1

**Design and Manufacturing Group**

## Question 2.1

![Matlab](images/Q1.png)

**Script:** `script_q2_1.m`

## Question 2.2

### 2.2.1

#### Line Charts

![Figure 2.2.1a](images/figure_2-i-Total.png)

![Figure 2.2.1b](images/figure_2-i-Weekly.png)

**Script:** `script_q2_2_1_linecharts.py`

#### Line Graphs

**For the whole period weekly consumption:**

![Figure 2.2.1c](images/Figure_2_Weekly_Consumption.png)

**For the whole period daily consumption:**

![Figure 2.2.1c](images/Figure_2_Daily_Consumption.png)

**Daily consumption over one week:**

![Figure 2.2.1d](images/Figure_2_Daily_Consumption_1Week.png)

**Script:** `script_q2_2_1_linecharts.py`

#### Bar Charts

![Figure 2.2.1e](images/221_bar_all_weekly.png)

![Figure 2.2.1f](images/221_bar_all_daily.png)

![Figure 2.2.1g](images/221_bar_week_daily.png)

**Script:** `script_q2_2_1_bar.py`

### 2.2.2

![Figure 2.2.2](images/222_heatmap.png)

**Script:** `script_q2_2_2.py`

### 2.2.3

![Figure 2.2.3](images/223_histogram.png)

**Script:** `script_q2_2_3.py`

### 2.2.4

![Figure 2.2.4](images/224_nsm_scatter.png)

**Script:** `script_q2_2_4.py`

### 2.2.5

![Figure 2.2.5](images/225.png)

**Script:** `script_q2_2_5.py`

### 2.2.6

The energy consumption from this data set seems to be quite evenly distributed. Based on the scatter plots we generated of the raw data, it would be difficult
to resolve a relationship between Energy Consumption and NSM or Pressure or determine the significance their contribution to energy consumption.

The data shown in the NSM scatter plot looks extremely uniform, indicating no significant trend. Intuitively, we would expect some sort of periodic trend over 
the course of the day that would increase in the morning and around 5pm or 6pm and then decrease between 10pm and midnight. It is possible that some data processing
and correlation analyses might allow us to resolve a relationship for the contribution of this factor.

The data shown in the pressure scatter plot shows a concetration in the pressures from approximately 749 to 766, but there does not seem to be any significant
trend with the energy consumption data. Again, we might be able to resolve some sort of trend if we processed or visualized the data in a different way.

## Question 3

| |Frequency (Hz)|Angle of Attack (degrees)|Chord Length (meters)|Free-stream Velocity (m/s)|SSD Thickness (meters)|Scale Sound Pressure Level (dB)|
|---|---|---|---|---|---|---|
|Mean|2886.3806|6.7823|0.1365|50.8607|0.0111|124.8359|
|Standard Deviation|3151.5242|5.9162|0.0935|15.5676|0.0131|6.8964|
|Median|1600.0000|5.4000|0.1016|39.6000|0.0050|125.7210|
|Kurtosis|5.6857|-0.4156|-1.0385|-1.5627|2.2075|-0.3171|
|Skewness|2.1350|0.6885|0.4570|0.2356|1.7005|-0.4185|
|Range|19800.0000|22.2000|0.2794|39.6000|0.0580|37.6070|

**Script:** `script_q2_3.py`
