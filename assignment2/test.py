import os
from hw2 import geometry_parser as gp
from hw2 import whitening as wt
from hw2 import tsne
import numpy as np

# Prepare paths for reading
input_geo_dir = os.path.dirname(os.path.realpath(__file__)) + "/input_geometry"
final_geo_dir = os.path.dirname(os.path.realpath(__file__)) + "/final_geometry"

# Read input and final geometries (time_step can be 80 or 140)
data = gp.prepare_geometry_data(input_geo_dir, final_geo_dir, time_step=80)

# Structure of "data":
#
# data[run_number][0] = all final geometry set for that run
#   data[run_number][0][x][0] = list of 3 surfaces
#   data[run_number][0][x][0][0] = equivalent of smesh.1.1.txt for final geometry as (x, y, z) data
#   data[run_number][0][x][1] = temperature
#   data[run_number][0][x][2] = pressure
#
# data[run_number][1] = input geometry set
#   data[run_number][1][0] = contents of smesh.1.1.txt for that run
#   data[run_number][1][2] = contents of smesh.1.3.txt for that run
#
# Note: The format described in the assignment assumes that control points are in v-order, but it reality they are not.
#       There is no need to get scared about this problem because, all arrays are consistent in themselves, and
#       perfectly fits to the structure given in the question.
#       Additionally, we are not expected to plot the given surfaces (heart valves) but we can! :)

# PCA, ZCA and TSNE code here
data_pca = wt.pca(data, measure_time=True)
tsne_results_pca = tsne.tsne(data_pca)

# data_zca = wt.zca(data, measure_time=True)
# tsne_results_zca = tsne.tsne(data_zca)

# Plotting code here (it would be good to use scatter plot)

# Put something non-functional here for the debugger
pass
